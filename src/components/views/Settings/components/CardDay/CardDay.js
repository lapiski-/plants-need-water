import React, {useState} from 'react';
import styled from 'styled-components';

const Day = styled.h4`
    background: #f4f7fa;
    padding: 15px;
    border-radius: 6px;
    text-transform: uppercase;
    font-size: 12px;
    color: #a0a6aa;
    font-weight: bold;
    &.on {
        background: #3d5051;
        color: #f4f7fa;
    }
`;


function CardDay() {

    //const weekend = useContext(ModeTest);

    const [weekend, setWeekend] = useState([
        {"day": "Monday", "enable":false},
        {"day": "Tuesday", "enable":false},
        {"day": "Wednesday", "enable":true},
        {"day": "Thursday", "enable":false},
        {"day": "Friday", "enable":false},
        {"day": "Saturday", "enable":false},
        {"day": "Sunday", "enable":false},
    ]);

    const handleDay = (e) => {
        setWeekend(weekend.map(itemDay => {
            return itemDay.day == e.currentTarget.innerHTML ? {...itemDay, "enable":!itemDay.enable} : itemDay;
        }));
    };

    return (
        weekend.map((itemDay, i)=> (      
            <Day className={`${itemDay.enable ? 'on' : 'off'}`} key={i} onClick={handleDay}>{itemDay.day}</Day>
        ))
    );
}


export default CardDay;