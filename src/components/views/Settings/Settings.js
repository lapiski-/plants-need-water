import React from "react";
import CardDay from './components/CardDay';
import IconBack from '@material-ui/icons/KeyboardBackspace';
import styled from 'styled-components';
import { useHistory } from "react-router-dom";
import {AnimatePresence} from "framer-motion";

import AnimationWrapper from 'components/reusable-components/AnimationWrapper';

const Head = styled.div`
    display: flex;
    flex-direction: column;
    align-items: baseline;
    margin: 20px 0px 20px 0px;
    svg { 
        margin-bottom: 10px;
        color: #a5abaf;
        font-size: 30px;
        margin-left: -2px;
    }
`;
const Title = styled.h1`
    margin: 0px;
    font-size: 24px;
    color: #4b5256;;
    font-weight: bold;
`;
const Body = styled.div`
    h2 {
        text-transform: uppercase;
        font-size: 15px;
        color: #a0a6aa;
        font-weight: bold;
    }
`;

function Settings() {

    const history = useHistory();

    const handleBack = () => {
        history.push(`/plants-need-water`);
    };

    return (
        <AnimationWrapper animation={['slideInRight', 'slideOutRight']}>
            <AnimatePresence>
            <div className="container-settings"> 
                <Head>
                    <IconBack className="icon-back" onClick={handleBack}/>
                    <Title>Settings</Title>
                </Head>
        
                <Body>
                    <h2>Water Days</h2>
                    <CardDay/>
                </Body>
            </div>
            </AnimatePresence>
        </AnimationWrapper>
    );
}

export default Settings;