import React, {useState} from 'react';
import Plant1 from 'images/plant-1.png';
import IconWater from '@material-ui/icons/Opacity';
import IconCheck from '@material-ui/icons/Check';
import IconCap from '@material-ui/icons/LocalDrink';
import styled from 'styled-components';

const Name = styled.h3`
  font-size: 16px;
  color: #ffffff;
  font-weight: bold;
  margin-top: 0px;
  z-index: 9;
`;
const Card = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 80px;
    box-sizing: border-box;
    padding: 15px;
    margin-bottom: 15px;
    border-radius: 10px;
    background: #e3e8e9;
    &.on {
        &:nth-child(4n+1) {
           background: #a9cde4;
        }
        &:nth-child(4n+2) {
            background: #7cb6d2;
        }
        &:nth-child(4n+3) {
            background: #b9dedd;
        }  
        &:nth-child(4n+4) {
            background: #87bac8;
        }
        span {
            color: white;
        }
    }
    .info {
        display: flex;
        width: 60%;
        flex-direction: column;
        height: 100%;
        justify-content: space-between;
    }
    img {
        max-height: 60px;
        margin-right: 20px;
    }
    .icon-watter {
        background: white;
        color: #a9d4e4;
        padding: 5px;
        border-radius: 50%;
        font-size: 30px;
    }
    .icon-check {
        background-color: rgba(255,255,255,0.5);
        color: white;
        padding: 5px;
        border-radius: 50%;
        font-size: 30px;
    }
    span {
        display: flex;
        align-items: center;
        color: #4b5256;
        
    }
`;


function CardWater({plant}) {

    const [check,setCheck] = useState(false);


    const handleCardWater = () => {
        setCheck(!check);
    };

    return (
        
        <Card data-check={check} onClick={handleCardWater} className={`${check ? 'on' : 'off'}`}>
            <img src={Plant1}></img>
            <div className="info">
                <Name>{plant.title}</Name>
                <span><IconCap/> - {plant.properties.water.subcategory}</span>
            </div>
            {check === false
                ?
                <IconWater className="icon-watter"/>
                : 
                <IconCheck className="icon-check"/>
            } 
        </Card>
    );
}


export default CardWater;