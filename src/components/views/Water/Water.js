import React, {useState, useEffect} from 'react';
import {AnimatePresence} from "framer-motion";
import CardWater from './components/CardWater/index';
import Nav from 'components/reusable-components/Nav';
import IconSettings from '@material-ui/icons/Settings';
import styled from 'styled-components';
import apiClient from 'utils/apiClient';
import { useHistory } from "react-router-dom";

import AnimationWrapper from 'components/reusable-components/AnimationWrapper';

const WrapperIcon = styled.div`
    display: flex;
    justify-content: flex-end;
    margin-top: 20px;
    color: #a5abaf;
`;
const Head = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 20px 0px 20px 0px;
`;
const Title = styled.h1`
    margin: 0px;
    font-size: 24px;
    color: #4b5256;;
    font-weight: bold;
`;

function Water() {

    const [plants, setPlants] = useState([]);
    const history = useHistory();

    useEffect(() => {
        apiClient.get('http://localhost:3001/api/plants/').then((data) => {
            setPlants(data);
        });



    }, [history]);

    const handleSettings = () => {

        history.push(`/plants-need-water/settings`);
    };


    return (
        <AnimationWrapper animation={["static","static"]}>
            <AnimatePresence>
                <div className="container-water">
                    <Nav/>
                    <div className="container with-nav"> 
                        <WrapperIcon>
                            <IconSettings onClick={handleSettings} className="icon"/>
                        </WrapperIcon>
                        <Head>
                            <Title>Watter Today</Title>
                            <span>{plants.filter(plant => plant.myplant == true).length} Plants</span>
                        </Head>
                        {plants.filter(plant => plant.myplant == true).map(itemPlant => {
                            return  <CardWater plant={itemPlant}  key={itemPlant.id}/>;
                        })}
                    </div>
                </div> 
            </AnimatePresence>
        </AnimationWrapper>
    );
}

export default Water;