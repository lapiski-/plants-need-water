import React, {useState, useEffect} from 'react';
import CardPlants from './components/CardPlants';
import IconBack from '@material-ui/icons/KeyboardBackspace';
import styled from 'styled-components';
import apiClient from 'utils/apiClient';
import { useHistory } from "react-router-dom";
import { useParams } from 'react-router-dom';

function SubCategory() {

    const Wrapper = styled.div`
        display: flex;
        flex-wrap: wrap;
        margin: -2%;
    `;

    const [plants, setPlants] = useState([]);
    const params = useParams();
    const history = useHistory();

    const handleGoBack = () => {
        history.push(`/plants/`);
    };

    useEffect(() => {
        apiClient.get('http://localhost:3001/api/plants/').then((data) => {
            setPlants(data);
        });
    }, [setPlants]);

    // const calculo = (plants) => {
    //     var test = plants.filter(plant => plant.properties[params.category].subcategory === params.subcategory);
    //     return test.map((item) => {
    //         console.log("item ==>",item);
    //         return <CardPlants plant={item} key={item.id}/>;
    //     });
    // };

    return (
        <div className="container"> 
            <IconBack className="icon-back" onClick={handleGoBack}/>

            <h3>Category {params.category} | Subcategory {params.subcategory}</h3>

            <Wrapper>
                {plants.filter(plant => plant.properties[params.category].subcategory === params.subcategory).map(itemPlant => {
                    return <CardPlants plant={itemPlant} key={itemPlant.id}/>; 
                })}
                {/* {calculo(plants)} */}
            </Wrapper>
            
        </div>
    );
}

export default SubCategory;