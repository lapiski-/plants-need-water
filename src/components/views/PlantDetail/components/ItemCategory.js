import React from 'react';
import styled from 'styled-components';
import IconSun from '@material-ui/icons/WbSunny';
import IconWater from '@material-ui/icons/Opacity';
import IconSize from '@material-ui/icons/Height';
import IconDifficult from '@material-ui/icons/Settings';
import IconTemperature from '@material-ui/icons/Whatshot';

//import ItemSun from 'components/reusable-components/ItemSun';


const Item = styled.section`
    display: flex;
    background: #f4f7fa;
    border-radius: 4px;
    padding: 15px;
    margin-bottom: 10px;
    border: 1px solid #def0f9;
    .icon, svg {
        width: 20px;
        margin-right: 15px;
        color: #a0a6aa;
    }
    .wrapper-info {
        width: 100%;
        .wrapper-header {
            display: flex;
            justify-content: space-between;
        }
    }
    .category, .options {
        text-transform: uppercase;
        font-size: 12px;
        color: #a0a6aa;
        font-weight: bold;
    }
    .options {
        span + span {
            margin-left: 10px;
        }
        .selected {
            background: #7cb6d2;
            color: white;
            padding: 3px;
            border-radius: 50%;
            width: 15px;
            display: inline-block;
            text-align: center;
            height: 15px;
        }
    }
    .description {
        color: #414042;
        font-size: 14px;
        margin: 5px 0px 0px 0px;
        text-transform: capitalize;
    }

`;

const IconCategory = (category) => {
    switch(category) {
        case 'size':
            return <IconSize/>;
        case 'difficulty':
            return <IconDifficult/>;
        case 'water':
            return <IconWater/>;
        case 'light':
            return <IconSun/>;
        case 'temperature':
            return <IconTemperature/>;
        default:
            return 'null';
    }
};


const ArrayNumber = [1, 2, 3];


function ItemCategory({property,category}) {
    console.log("property",property);
    console.log("test ==>",category);
    return (  
        <Item>
           {IconCategory(category)}
            <div className="wrapper-info">
                <div className="wrapper-header">
                    <span className="category">{category}</span>
                    <div className="options">
                        {ArrayNumber.map((arrayCat, i) => {
                            if (ArrayNumber[i] === property.position) {
                                return  <span className="selected" key={i}>{arrayCat}</span>;
                            } 
                            else {
                                return  <span key={i}>{ArrayNumber[i]}</span>;
                            }
                        })}
                    </div>
                </div>
                <p className="description">{property.subcategory}. {property.description}</p>
            </div>
        </Item>
    );
}



export default ItemCategory;