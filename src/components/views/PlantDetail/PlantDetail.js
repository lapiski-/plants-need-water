import React, {useState, useEffect} from 'react';
import Plant1 from 'images/plant-1.png';
import IconFavoriteOff from '@material-ui/icons/FavoriteBorder';
import IconFavorite from '@material-ui/icons/Favorite';
import ItemCategory from './components/ItemCategory';
import IconBack from '@material-ui/icons/KeyboardBackspace';
import ButtonMore from 'components/reusable-components/ButtonMore';
import ButtonCheck from 'components/reusable-components/ButtonCheck';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import apiClient from 'utils/apiClient';
//import axios from 'axios';


function PlantDetail() {
    const WrapperHead = styled.div`
        padding: 30px 20px 25px 20px;
        border-radius: 15px;
        margin: 15px 15px 70px 15px;
        &:nth-child(1n) {
            background: #a9cde4;
        }
        &:nth-child(2n) {
            background: #7cb6d2;
        }
        &:nth-child(3n) {
            background: #b9dedd;
        }
        &:nth-child(4n) {
            background: #87bac8;
        }
        .icon-back {
            font-size: 25px;
            color: white;
            margin-bottom: 12px;
            position: relative;
            left: -2px;
        }
        .wrapper-img {
            display: flex;
            justify-content: flex-end;
        }
        .add-plant {
            display: flex;
            flex-direction: column;
            align-items: center;
            position: absolute;
            left: 0;
            width: 100%;
            svg {
                padding: 9px;
                margin-bottom: 5px;
            }
            .icon-add {
                svg {
                    background: #3c4f51;
                }
            }
            span {
                text-transform: uppercase;
                font-size: 12px;
                color: #a0a6aa;
                font-weight: bold;
            }
        }
    `;
    const Head = styled.div`
        display: flex;
        justify-content: space-between;
        align-items: center;
        color: white;
        align-items: end;
    `;
    const Name = styled.div`
        font-size: 28px;
        font-weight: bold;
        max-width: 50%;
    `;
    const WrapperBody = styled.div`
        .wrapper-categories {
            margin-top: 30px;
        }
    `;

    const [plant, setPlant] = useState({});
    const params = useParams();
    const history = useHistory();

    useEffect(() => {
        apiClient.get(`http://localhost:3001/api/plants/${params.id}`).then((data) => {
            //console.log("data ==>",data);
            return setPlant(data);
        });
    }, [params.id]);

    const handleGoBack = () => {
        history.push(`/plants/`);
    };

    async function handleAddFavourite() {
        // try{
        //     apiClient.put(`http://localhost:3001/api/plants/${params.id}`,JSON.stringify({"favorite": true})).then((data) => console.log("data ==>",data));
        // }catch(e){
        //     console.log("error ===>",e);
        // }
        //const res = await axios.put(`http://localhost:3001/api/plants/${params.id}`, { favorite: true });
       // console.log("res ==>",res);
    }

    return (
            <React.Fragment>
            <WrapperHead id={plant.id}> 
                <IconBack className="icon-back" onClick={handleGoBack}/>
                <Head>
                    <Name>{plant.title}</Name>
                    {plant.favorite === true
                        ?
                        <IconFavorite onClick={handleAddFavourite}/> 
                        : 
                        <IconFavoriteOff onClick={handleAddFavourite}/>
                    } 
                </Head>
                <div className="wrapper-img">
                    <img src={Plant1}></img>
                </div>
                <div className="add-plant">
                {plant.myplant 
                    ?
                    <React.Fragment>
                        <ButtonCheck/>
                        <span>Added</span>
                    </React.Fragment>   
                    : 
                    <React.Fragment>
                        <ButtonMore/>
                        <span>Add to my plants</span>
                    </React.Fragment>
                } 
                </div>
            </WrapperHead>
            <WrapperBody className="container">
                <h4>Description</h4>
                <p>{plant.description}</p>
                <div className="wrapper-categories">
                    {plant && plant.properties && Object.keys(plant.properties).map(function(property,key) {
                        //console.log("======>",property,plant.properties[property]);
                        return <ItemCategory plant={plant} category={property} property={plant.properties[property]} key={key}></ItemCategory>;
                    }) }
                </div>
            </WrapperBody>
        </React.Fragment> 
    );

    
}

export default PlantDetail;