import React, {useState, useEffect} from 'react';
import CardPlants from './components/CardPlants';
import MenuPlants from './components/MenuPlants';
import Nav from 'components/reusable-components/Nav';
import Search from './components/Search';
import styled from 'styled-components';
import { useHistory } from "react-router-dom";
import apiClient from 'utils/apiClient';


function DistributorPlants() {

    const Head = styled.div`
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin: 40px 0px 20px 0px;
        .number {
            text-transform: uppercase;
            font-size: 12px;
            color: #a0a6aa;
            font-weight: bold;
        }
    `;
    const Category = styled.h2`
        font-size: 18px;
        color: #4b5256;;
        font-weight: bold;
        text-transform: capitalize;
    `;
    const Wrapper = styled.div`
        display: flex;
        flex-wrap: wrap;
        margin: -2%;
    `;
    const categoryProperties = {
        "water":["150ml", "250ml", "500ml"], 
        "size":["small", "medium", "large" ], 
        "difficulty":[ "easy", "medium", "difficult"], 
        "light":["low", "medium", "long"], 
    };

    const [plants, setPlants] = useState([]);
    const [categorySelect, setCategorySelect] = useState('water');
    const history = useHistory();

    useEffect(() => {
        apiClient.get('http://localhost:3001/api/plants/').then((data) => {
            setPlants(data);
        });
    }, []);

    const handledataCategory = (e) => {
        setCategorySelect(e);
    }; 

    const handleTest = (e) => {
        const categoryTest = categorySelect;
        const subcategoryTest = e.target.getAttribute("data-subcategory");
        history.push(`/plants/${categoryTest}/${subcategoryTest}`);
    };
    
   
    return (
        <div className="container with-nav"> 
            <Nav/>
            <Search/>
            <MenuPlants dataCategory={handledataCategory} />

            {categoryProperties[categorySelect].map((subCategory, i) => {
                return  (
                    <div key={i}>
                        <Head>
                            <Category>{subCategory}</Category>
                            <span data-subcategory={subCategory} className="number" onClick={handleTest}>Show all {plants.filter(plant => plant.properties[categorySelect].subcategory === subCategory && plant.myplant == false).length}</span>
                        </Head>
                        <Wrapper>
                            {plants.filter(plant => plant.properties[categorySelect].subcategory  === subCategory && plant.myplant == false).slice(0,4).map(itemPlant => {
                                return  <CardPlants plant={itemPlant} key={itemPlant.id}/>; 
                            })}
                        </Wrapper>
                    </div>
                );
            })}
        </div>
    );
}

export default DistributorPlants;