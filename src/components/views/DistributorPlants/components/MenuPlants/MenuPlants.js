import React, {useState,useEffect} from 'react';
import styled from 'styled-components';

//const prueba = test;
//left: ${prueba};

const Menu = styled.ul`
    padding: 0px;
    margin: 30px 0px;
    display: flex;
    justify-content: space-between;
    position: relative;
    padding-bottom: 8px;
    position: relative;
    &:before {
        content:"";
        position: absolute;
        width: 100%;
        height: 1px;
        bottom: 0;
        background: #a0a6aa;
    }
    li {
        list-style: none;
        text-transform: uppercase;
        font-size: 12px;
        color: #a0a6aa;
        font-weight: bold;
        &.active {
            color: #4b5256;

        }
    }
    #buttonActive {
        position: absolute;
        width: 5px;
        height: 5px;
        border-radius: 50%;
        background: #4b5256;
        bottom: -2px;
        left: 0;
        transition: all 0.8s;
    }
`;

function CardPlants({dataCategory}) {

    const [itemMenuSelected, setItemMenuSelected] = useState("water");

    const handleSelectCategory = (e) => {
        //alert(e.target.getAttribute("data-category"));
        setItemMenuSelected(e.target.getAttribute("data-category"));
        return dataCategory(e.target.getAttribute("data-category"));
    };

    useEffect(function() {
        const buttonActive = document.getElementById("buttonActive");
        const elementActive = document.getElementById("active");
        const left = elementActive.offsetLeft;
        const widtElementActive = elementActive.offsetWidth;
        buttonActive.style.left = `${left + widtElementActive/2 - 3}px`;
    });

    const menuCategory = ["water", "size", "difficulty", "light"];

    return (
        <Menu id="test">
            {menuCategory.map((cat,i) => {
                return <li key={i} data-category={cat} onClick={handleSelectCategory} id={`${itemMenuSelected == menuCategory[i] ? 'active' : ''}`} className={`${itemMenuSelected == menuCategory[i] ? 'active' : ''}`}>{cat}</li>;
            })}
            <span id="buttonActive"></span>
        </Menu>
    );
}



export default CardPlants;