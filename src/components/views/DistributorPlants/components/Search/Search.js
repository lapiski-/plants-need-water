import React from 'react';
import styled from 'styled-components';

const SearchButton = styled.div`
    margin-top: 30px;
    background: #f5fafe;
    border: 2px solid #e3ebf1;
    border-radius: 20px;
    padding: 7px 15px;
    text-align: center;
    color: #a0a6aa;
    font-weight: bold;
    font-size: 15px;
`;

function Search() {

    return (

        <SearchButton>
            Search
        </SearchButton>
 
    );
}



export default Search;