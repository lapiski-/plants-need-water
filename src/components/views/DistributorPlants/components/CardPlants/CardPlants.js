import React from 'react';
import ButtonMore from 'components/reusable-components/ButtonMore';
import Plant1 from 'images/plant-1.png';
//import Plant2 from 'images/plant-2.png';
//import Plant3 from 'images/plant-3.png';
import { useHistory } from "react-router-dom";
import styled from 'styled-components';

const Name = styled.h3`
  font-size: 16px;
  color: #ffffff;
  font-weight: bold;
  margin-top: 0px;
  z-index: 9;
`;
const Card = styled.div`
    position: relative;
    width: 46%;
    min-height: 165px;
    box-sizing: border-box;
    padding: 10px;
    margin: 2% ;
    border-radius: 10px;
    &:nth-child(1n) {
        background: #a9cde4;
    }
    &:nth-child(2n) {
        background: #7cb6d2;
    }
    &:nth-child(3n) {
        background: #b9dedd;
    }
    &:nth-child(4n) {
        background: #87bac8;
    }
    .left {
        display: flex;
        width: 60%;
        flex-direction: column;
        height: 100%;
        justify-content: space-between;
    }
    img {
        max-width: 55%;
        max-height: 85%;
        position: absolute;
        right: 10px;
        bottom: 10px;
    }
`;

function CardPlants({plant}) {

    const history = useHistory();

    const handleDetailPlant = (plant) => {
        const idPlantSelected = plant.id;
        history.push(`/plant/${idPlantSelected}`);
    };
    

    return (
        <Card id={plant.id}>
            <div className="left">
                <Name onClick={() => handleDetailPlant(plant)}>{plant.title}</Name>
                <ButtonMore/>
            </div>
            <img src={Plant1}></img>
        </Card>
    );
}



export default CardPlants;