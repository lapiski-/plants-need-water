import React, {useState, useEffect} from 'react';
import CardMyPlants from './components/CardMyPlants';
import Nav from 'components/reusable-components/Nav';
import styled from 'styled-components';
import FavoriteOff from '@material-ui/icons/FavoriteBorder';
import apiClient from 'utils/apiClient';

function MyPlants() {

    const [plants, setPlants] = useState([]);

    useEffect(() => {
        apiClient.get('http://localhost:3001/api/plants/').then((data) => {
            console.log("data books ==>",data);
            setPlants(data);
        });
    }, [setPlants]);

    const Head = styled.div`
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin: 40px 0px 20px 0px;
    `;
    const Title = styled.h1`
        margin: 0px;
        font-size: 24px;
        color: #4b5256;;
        font-weight: bold;
    `;

    // valuesWater = [];

    return (
 
        <div className="container with-nav"> 
            <Nav/>
            <Head>
                <Title>My Plants</Title>
                <FavoriteOff/>
            </Head>
            
            {plants.filter(plant => plant.myplant).map(itemPlant => {
                // valuesWater.push(itemPlant.properties.water.position);
                // console.log("valuesWater-->", valuesWater);
                // const maxValueWater = Math.max(...valuesWater);
                // console.log("maxValueWater-->", maxValueWater);

                return <CardMyPlants plant={itemPlant} key={itemPlant.id}/>;
            })}
            
        </div>
    );
}

export default MyPlants;


