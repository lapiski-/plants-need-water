import React from 'react';
import Plant1 from 'images/plant-1.png';
// import Plant2 from 'images/plant-2.png';
// import Plant3 from 'images/plant-3.png';
import styled from 'styled-components';
import ItemSun from 'components/reusable-components/ItemSun';
import ItemWater from 'components/reusable-components/ItemWater';
import ItemTemperature from 'components/reusable-components/ItemTemperature';
import { useHistory } from "react-router-dom";


const Name = styled.h3`
    font-size: 22px;
    color: #ffffff;
    font-weight: bold;
    margin-top: 0px;
    z-index: 9;
`;

const Card = styled.section`
    position: relative;
    width: 100%;
    min-height: 165px;
    box-sizing: border-box;
    padding: 35px 20px;
    border-radius: 10px;
    overflow: hidden;
    margin-bottom: 15px;
    &:nth-child(4n+1) {
        background: #a9cde4;
    }
    &:nth-child(4n+2) {
        background: #7cb6d2;
    }
    &:nth-child(4n+3) {
        background: #b9dedd;
    }
    &:nth-child(4n+4) {
        background: #87bac8;
    }
    .left {
        display: flex;
        width: 60%;
        flex-direction: column;
        height: 100%;
        justify-content: space-between;
        .wrapper-items {
            z-index: 9;
            margin-top: 50px;
            >div {
                margin-bottom: 15px;
                &:last-child {
                    margin-bottom: 0px;
                }
            }
        }
    }
    .img-plant {
        max-width: 55%;
        max-height: 80%;
        position: absolute;
        right: -19px;
        bottom: 30px;
    }
`;

function CardMyPlants({plant}) {

    const history = useHistory();

    const handleDetailPlant = (plant) => {
        const idPlantSelected = plant.id;
        history.push(`/plant/${idPlantSelected}`);
    };

    return (  
        <Card>
            <div className="left">
                <Name onClick={() => handleDetailPlant(plant)}>{plant.title}</Name>
                <div className="wrapper-items">
                    <ItemWater plant2={plant}/>
                    <ItemSun plant2={plant}/>
                    <ItemTemperature plant2={plant}/>
                </div>
            </div>
            <img className="img-plant" src={Plant1}></img>
        </Card>
    );
}



export default CardMyPlants;