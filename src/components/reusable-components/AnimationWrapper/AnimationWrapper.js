import React from 'react';
import PropTypes from 'prop-types';
import { motion } from 'framer-motion';

class AnimationWrapper extends React.Component {
    prepareAnimations() {
        const animationStart = this.props.animation[0],
            animationEnd = this.props.animation[1];

        var pageVariants = {
            initial: { zIndex: 5 },
            in: { x: 0, y: 0 },
            out: { zIndex: animationEnd ? 6 : 5 },
        };

        if (animationStart) {
            switch (animationStart) {
                case 'slideInRight':
                    pageVariants.initial.zIndex = 6;
                    pageVariants.initial.x = '100%';
                    break;
                case 'slideInLeft':
                    pageVariants.initial.x = '-100%';
                    break;
                case 'slideInUp':
                    pageVariants.initial.zIndex = 10;
                    pageVariants.initial.y = '100%';
                    break;
                default: {
                    pageVariants.initial.zIndex = 5;
                    pageVariants.initial.opacity = 1;
                }
            }
        }

        if (animationEnd) {
            switch (animationEnd) {
                case 'slideOutRight':
                    pageVariants.out.x = '100%';
                    break;
                case 'slideOutLeft':
                    pageVariants.out.x = '-100%';
                    break;
                case 'slideOutDown':
                    pageVariants.out.y = '100%';
                    break;
                default: {
                    pageVariants.out.opacity = 1;
                }
            }
        }

        return pageVariants;
    }

    render() {
        return (
            <motion.div
                className={this.props.className}
                ref={this.props.refTest}
                initial="initial"
                animate="in"
                exit="out"
                variants={this.prepareAnimations()}
                transition={{ duration: .5 }}
                style={this.props.style}
                onClick={this.props.onClick}
            >
                {this.props.children}
            </motion.div>
        );
    }
}

export default AnimationWrapper;

AnimationWrapper.propTypes = {
    className: PropTypes.string.isRequired,
    animation: PropTypes.array.isRequired,
};
