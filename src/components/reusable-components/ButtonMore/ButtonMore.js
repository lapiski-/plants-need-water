import React from 'react';
//import IconMore from '@material-ui/icons/AddCircle';
import IconMore from '@material-ui/icons/Add';
import styled from 'styled-components';


const Button = styled.span`
    svg {
        color: white;
        background-color: rgba(255,255,255,0.35);
        border-radius: 50%;
        padding: 4px;
        font-size: 24px;
    }
`;


function ButtonMore() {

    return (
        <Button  className="icon-add">
            <IconMore/>
        </Button>
    );
}



export default ButtonMore;