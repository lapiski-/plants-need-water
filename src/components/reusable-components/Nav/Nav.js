import React from "react";
import {NavLink} from "react-router-dom";
import {HOME, MYPLANTS, WATER} from 'config/routes';
import home from './home.svg';
import watering from './watering.svg';
import plants from './plant.svg';

function Nav() {
    return (
        <div className="container">
            <header>
                <nav className="main-nav">
                    <ul>
                        <li>
                            <NavLink to={HOME} activeClassName="active" exact>
                                <img src={home} className="home" alt="home" /> 
                                <span>General</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to={WATER} activeClassName="active" exact>
                                <img src={watering} className="watering" alt="watering" /> 
                                <span>Regar</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to={MYPLANTS} activeClassName="active" exact>
                                <img src={plants} className="plants" alt="plants" /> 
                                <span>Mis plantas</span>
                            </NavLink>
                        </li>
                    </ul>
                </nav>
            </header>
        </div>
    );
}

export default Nav;