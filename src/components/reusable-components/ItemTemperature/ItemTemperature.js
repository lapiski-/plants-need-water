import React from 'react';
import styled from 'styled-components';
//import Thermometer from 'images/thermometer.svg';
import IconTemperature from '@material-ui/icons/Whatshot';


const ItemButton = styled.div`
    color: white;
    display: flex;
    svg, .icon {
        font-size: 20px;
        margin-right: 5px;
        width: 20px;
    }
    div {
        display: flex;
        flex-direction: column;
        span {
            font-size: 13px;
        }
        .category {
            text-transform: uppercase;
            letter-spacing: 1px;
        }
    }
`;


function ItemTemperature({plant2}) {

    return (
        <ItemButton className="item" key={plant2.id}>
            {/* <img className="icon" src={Thermometer}></img> */}
            <IconTemperature/>
            <div>
                <span className="category">Temperature</span>
                <span>{plant2.properties.temperature.description}</span>
            </div>
        </ItemButton>
    );
}



export default ItemTemperature;