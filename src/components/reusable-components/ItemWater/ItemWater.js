import React from 'react';
import styled from 'styled-components';
import IconWater from '@material-ui/icons/Opacity';


const ItemButton = styled.div`
    color: white;
    display: flex;
    svg {
        font-size: 20px;
        margin-right: 5px;
    }
    div {
        display: flex;
        flex-direction: column;
        span {
            font-size: 13px;
        }
        .category {
            text-transform: uppercase;
            letter-spacing: 1px;
        }
    }
`;


function ItemWater({plant2}) {

    return (
        <ItemButton className="item">
            <IconWater/>
            <div>
                <span className="category">Water</span>
                <span>{plant2.properties.water.position} / week</span>
            </div>
        </ItemButton>
    );
}



export default ItemWater;