import React from 'react';
import IconCheck from '@material-ui/icons/Check';
import styled from 'styled-components';


const Button = styled.span`
    svg {
        color: #75838a;
        background-color: #e7e8e8;
        border-radius: 50%;
        padding: 4px;
        font-size: 24px;
    }
`;


function ButtonCheck() {

    return (
        <Button className="icon-check">
            <IconCheck/>
        </Button>
    );
}



export default ButtonCheck;