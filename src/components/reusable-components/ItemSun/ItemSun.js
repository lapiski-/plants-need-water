import React from 'react';
import styled from 'styled-components';
import IconSun from '@material-ui/icons/WbSunny';

const ItemButton = styled.div`
    color: white;
    display: flex;
    svg {
        font-size: 22px;
        margin-right: 5px;
    }
    div {
        display: flex;
        flex-direction: column;
        span {
            font-size: 13px;
        }
        .category {
            text-transform: uppercase;
            letter-spacing: 1px;
        }
    }
`;


function ItemSun({plant2}) {

    return (
        <ItemButton className="item">
            <IconSun/>
            <div>
                <span className="category">Light</span>
                <span>{plant2.properties.light.subcategory}</span>
            </div>
        </ItemButton>
    );
}



export default ItemSun;