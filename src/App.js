import './App.css';
import React, {useState} from "react";
import {AnimatePresence} from "framer-motion";
import {
  Switch,
  Route,
  useLocation,
} from "react-router-dom";
import DistributorPlants from './components/views/DistributorPlants';
import MyPlants from './components/views/MyPlants';
import Water from './components/views/Water';
import PlantDetail from './components/views/PlantDetail';
import SubCategory from './components/views/SubCategory';
import Settings from './components/views/Settings';
import {HOME, MYPLANTS, WATER, PLANT_DETAIL, SUBCATEGORY, SETTINGS} from './config/routes';

import {ThemeProvider} from "styled-components";
import { GlobalStyles } from "./theme/global";
import { lightTheme, darkTheme } from "./theme/theme";


export default function App() {
    const location = useLocation();

    const [theme, setTheme] = useState('light');
    const themeToggler = () => {
        theme === 'light' ? setTheme('dark') : setTheme('light');
    };

    return (
        <ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
            <GlobalStyles/>
 
            <div className="container">
                <p>Tema actual: {theme}</p>
                <button onClick={themeToggler}>Cambiar theme</button>
            </div>
            
            <div className="content">
                <AnimatePresence>
                    <Switch key={location.pathname} location={location}>
                        <Route path={HOME} exact>
                            <DistributorPlants />
                        </Route>
                        <Route path={MYPLANTS} exact>
                            <MyPlants/>
                        </Route>
                        <Route path={WATER} exact>
                            <Water/>
                        </Route>
                        <Route path={PLANT_DETAIL} exact>
                            <PlantDetail/>
                        </Route>
                        <Route path={SUBCATEGORY} exact>
                            <SubCategory/>
                        </Route>
                        <Route path={SETTINGS} exact component={Settings} /> 
                    </Switch>
                </AnimatePresence>
            </div>
     
        </ThemeProvider>
    );
}