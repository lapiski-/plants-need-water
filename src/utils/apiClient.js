
const headers = {
    accept: 'application/json',
    'Content-Type': 'application/json,charset=UTF-8',
  };
  
  async function processResponse(response) {
    if (!response.ok) {
      throw new Error(`Request failed with ${response.status}`);
    }
    const json = await response.json();
    return json;
  }
  
  const apiClient = {
    get: async function(url) {
      const response = await fetch(url, {
        method: 'GET',
        headers
      });
      return await processResponse(response);
    },
    post: async function(url, body) {
       
      const response = await fetch(url, {
        method: 'POST',
        headers,
        body
      });
      return await processResponse(response);
    },
    put: async function(url, body) {
       console.log("body ==>",body);
       console.log("url ==>",url);
        const response = await fetch(url, {
          method: 'PUT',
          body
        });
        console.log("resp ==>",response);
        //return await processResponse(response);
        return await response.json();
      },
    del: async function(url) {
       await fetch(url, {
          method: 'DELETE',
          headers,
        });
      },
  };
  
  export default apiClient;