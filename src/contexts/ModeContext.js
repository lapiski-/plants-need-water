import { createContext } from "react";

// const ModeContext = createContext([
//     {"day": "Monday", "enable":false},
//     {"day": "Tuesday", "enable":false},
//     {"day": "Wednesday", "enable":true},
//     {"day": "Thursday", "enable":false},
//     {"day": "Friday", "enable":false},
//     {"day": "Saturday", "enable":false},
//     {"day": "Sunday", "enable":false},
// ]);

const ModeContext = createContext('light');

export default ModeContext;
