export const lightTheme = {
    body: 'red',
    text: 'green',
};

export const darkTheme = {
    body: '#2f2f2f',
    text: 'red',
};