export const LOGIN = '/';
export const LOGOUT = '/logout';
export const HOME = '/plants'; 
export const PLANT_DETAIL = '/plant/:id';
export const MYPLANTS = '/my-plants';
export const WATER = '/plants-need-water';
export const SETTINGS = '/plants-need-water/settings';
export const SUBCATEGORY = '/plants/:category/:subcategory';



