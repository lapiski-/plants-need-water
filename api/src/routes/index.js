const { Router } = require("express");
const router = Router();

const plants = require("../plants.json");

router.get("/", (req, res) => {
  res.json(plants);
});

module.exports = router;
