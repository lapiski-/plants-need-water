const { Router } = require("express");
const router = Router();

const plants = require("../plants.json");
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
router.get("/", (req, res) => {
  res.send(plants);
});

router.get("/:id", (req, res) => {
  const { id } = req.params;
  res.json(plants.find(plant => plant.id == id));
});

router.put("/:id", (req, res) => {
  const { id } = req.params;
  const { favorite } = req.body;

  if (favorite !== undefined) {
    const foundPlant = plants.find(plant => {
        return plant.id = id;
       
    });

    foundPlant.favorite = favorite;

    res.json(plants);
  } else {
    res.status(500).json({ error: "error 1" });
  }
});

module.exports = router;
