const express = require("express");
const app = express();
const morgan = require("morgan");

//settings
app.set("port", process.env.PORT || 3001);

// middlewares
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// routes
//app.use(require("./routes/index"));
app.use("/api/plants", require("./routes/plants"));

// starting the server
app.listen(app.get("port"), () => {
  console.log(`Server working on port ${3001}`);
});
